<section class="c-briefingBlock l-sec">
  <div class="l-wrap">
    <h2 class="c-secTtl02 u-colorWhite">バイリンガル限定！
      <br>会社説明会を開催します。
    </h2>
    <p class="u-colorWhite u-mb35">Kids Duoのこと、当社のことをもっと知っていただくために、バイリンガルを対象とした会社説明会を開催しています。是非ともお気軽にご参加ください。</p>
    <div class="btn-center">
      <div class="md-linktxt-btn02 color-white">
        <a href="https://pro.form-mailer.jp/fms/d3119aef92591" target="_blank">
          <span class="arrow-right">会社説明会に参加する</span>
        </a>
      </div>
    </div>
  </div>
</section>
<section class="l-sec u-bgGray">
  <div class="l-wrap">
    <h2 class="c-secTtl05"><span>Flow</span><small>採用までの流れ</small></h2>

    <div class="p-flowList">
      <div class="p-flowList_item js-matchHeight">
        <p class="p-flowList_item_header">STEP 1</p>
        <div class="p-flowList_item_body">
          <dl>
            <dt>エントリー</dt>
            <dd>専用フォームからエントリーしてください。転職サイトからのご応募も受け付けております。</dd>
          </dl>
        </div>
      </div><!-- /.p-flowList_item -->
      <div class="p-flowList_item js-matchHeight">
        <p class="p-flowList_item_header">STEP 2</p>
        <div class="p-flowList_item_body">
          <dl>
            <dt>書類選考</dt>
            <dd>履歴書、職務履歴書をお送りください。返却は行っておりませんので、予めご了承ください。</dd>
          </dl>
        </div>
      </div><!-- /.p-flowList_item -->
      <div class="p-flowList_item js-matchHeight">
        <p class="p-flowList_item_header">STEP 3</p>
        <div class="p-flowList_item_body">
          <dl>
            <dt>採用選考会 / 面接</dt>
            <dd>面接は、通常1〜2回を予定しています（職種により異なります）。</dd>
          </dl>
        </div>
      </div><!-- /.p-flowList_item -->
      <div class="p-flowList_item js-matchHeight">
        <p class="p-flowList_item_header">STEP 4</p>
        <div class="p-flowList_item_body">
          <dl>
            <dt>内定</dt>
            <dd>内定後に採用条件をご相談させていただきます。ご了承をいただいた後、入社日を決定します。</dd>
          </dl>
        </div>
      </div><!-- /.p-flowList_item -->
    </div><!-- /.p-flowList -->
    <dl class="c-simpleDef02">
      <dt>選考のポイント</dt>
      <dd>応募動機やこれまでの経験を重視しています。<br>業務実績や目標達成に至るプロセスなどがよりよく伝わるよう、具体的にご記載をお願いします。</dd>
    </dl>
  </div>
</section>
