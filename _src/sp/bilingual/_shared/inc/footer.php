<div class="c-conductorBox_wrap">
  <div class="l-wrap">
    <div class="c-conductorBox">
      <div class="c-conductorBox_img"><img src="/sp/bilingual/_shared/images/common/fotter_link_img.jpg" alt=""></div>
      <dl class="c-conductorBox_def">
        <dt>Kids Duoではバイリンガルスタッフ正社員・契約社員を随時募集しています。【未経験歓迎】</dt>
        <dd>KidsDuoでは、国籍も性別も超えて様々なスタッフが活躍中です。教室環境は常に英語だから、あなた自身の英語力も磨けます。「実際のレッスンはどうやってやるの？」「英語力はどれくらい必要なの？」お仕事内容に関するご質問など、お気軽にお問い合わせください。</dd>
      </dl>
      <div class="c-contactBox u-fntMeiryo">
        <p>バイリンガルスタッフ採用担当（直通）</p>
        <p class="c-contactBox_tel"><a href="tel:0355422758">03-5542-2758</a></p>
        <p>受付：平日 10:00 ~ 18:00</p>
      </div>
      <ul class="c-btns">
        <li class="c-btn02 c-btn02--red c-btn02--entry"><a href="https://rec-log.jp/site/jobVw.aspx?OU2qgXutI0WwP2ozE51BO82EgbuHIdWKagoNCjQPOm2SgpuVIrWYauo1CxQ3OA26gCu9IFWcaIoeCLQhOO2kgQunITWqaWosCZQvO12yg4uBe7yDdarGjdYJ" class="js-matchHeight" target="_blank"><span>エントリー</span></a></li>
        <li class="c-btn02 c-btn02--blue c-btn02--mail"><a href="/sp/bilingual/contact/" class="js-matchHeight"><span>お問い合わせ</span></a></li>
      </ul>
    </div>
  </div>
</div>
<footer class="l-footer">
  <ul class="l-footer_nav">
    <li><a href="/sp/bilingual/about/">Kids Duoについて</a></li>
    <li><a href="/sp/bilingual/working/">Kids Duoで働く</a></li>
    <li><a href="/sp/bilingual/interview/">先輩社員の声</a></li>
    <li><a href="/sp/bilingual/map/">スクール一覧</a></li>
    <li><a href="/sp/bilingual/recruit/">募集要項</a></li>
  </ul>
  <div class="l-footer_bottom">
    <p class="l-footer_logo"><a href="https://www.yarukiswitch.jp/" target="_blank"><img src="/sp/bilingual/_shared/images/common/footer_logo.jpg" alt="やる気スイッチグループ"></a></p>
    <p class="l-footer_bottom_txt">やる気スイッチグループは、個別指導塾のスクールIE、幼児教育のチャイルド・アイズ、英会話のウィンビー、英語で預かる学童保育・プリスクールのキッズデュオを全国で展開する総合教育グループです。</p>
    <p class="l-footer_bottom_img"><img src="/sp/bilingual/_shared/images/common/footer_img.jpg" alt="世界に広がる1378教室"></p>
    <ul class="c-inlineNav">
      <li><a href="https://www.yarukiswitch.jp/" target="_blank">やる気スイッチグループ</a></li>
      <li><a href="https://www.yarukiswitch.jp/privacypolicy.html" target="_blank">プライバシーポリシー</a></li>
    </ul>
    <p class="l-footer_copy">(c) Tact. Kodomomirai Co.Ltd. all rights reserved.</p>
  </div>
</footer>
