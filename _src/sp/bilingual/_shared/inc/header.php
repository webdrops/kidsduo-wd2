<header class="l-header">
  <p class="l-header_logo"><a href="/sp/bilingual/"><img src="/sp/bilingual/_shared/images/common/logo.svg" alt="英語で遊ぶ、学ぶ KidsDuo"></a></p>
  <p class="l-header_logogroup"><a href="https://www.yarukiswitch.jp/" target="_blank"><img src="/sp/bilingual/_shared/images/common/logo_group.png" alt="やる気スイッチグループ"></a></p>
  <div class="l-header_menu">
    <a href="#">
      <span></span>
      <span></span>
      <span></span>
    </a>
  </div>
  <div class="l-header_nav">
    <nav>
      <ul>
        <li><a href="/sp/bilingual/about/">Kids Duo について</a></li>
        <li><a href="/sp/bilingual/working/">Kids Duo で働く</a></li>
        <li><a href="/sp/bilingual/interview/">先輩社員の声</a></li>
        <li><a href="/sp/bilingual/map/">スクール一覧</a></li>
        <li><a href="/sp/bilingual/recruit/">募集要項</a></li>
        <li><a href="https://rec-log.jp/site/jobVw.aspx?OU2qgXutI0WwP2ozE51BO82EgbuHIdWKagoNCjQPOm2SgpuVIrWYauo1CxQ3OA26gCu9IFWcaIoeCLQhOO2kgQunITWqaWosCZQvO12yg4uBe7yDdarGjdYJ" target="_blank">エントリーフォーム</a></li>
        <li><a href="/sp/bilingual/contact/">お問い合わせ</a></li>
      </ul>
    </nav>
  </div>
</header>
<div class="c-conductorBtn">
  <ul>
    <li class="ico_entry"><a href="https://rec-log.jp/site/jobVw.aspx?OU2qgXutI0WwP2ozE51BO82EgbuHIdWKagoNCjQPOm2SgpuVIrWYauo1CxQ3OA26gCu9IFWcaIoeCLQhOO2kgQunITWqaWosCZQvO12yg4uBe7yDdarGjdYJ" target="_blank"><span>エントリー</span></a></li>
    <li class="ico_mail"><a href="/sp/bilingual/contact/"><span>お問い合わせ</span></a></li>
  </ul>
</div>
