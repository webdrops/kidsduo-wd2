$(function(){
  /*****************************************************
    ページスクロール処理
  *****************************************************/
  // PageTopスクロール
  /*--$('.js-pageTop a').click(function(){
    var elmID = '#l-wrapper';
    var posi = 0;
    if($(elmID).size()){
      posi = $(elmID).offset().top - $(".l-header").innerHeight();
      $('html,body').animate({
        scrollTop: posi
      }, 500);
      return false;
    }
  });--*/

  // ページ内スクロール
  /*--$('a[href^="#"]').not(".js-pageTop a").click(function(){
    var elmID = $(this).attr("href");
    if($(elmID).size()){
      posi = $(elmID).offset().top;
      $("html,body").animate({
        scrollTop: posi
      }, 500);
      return false;
    }
  });--*/

  /*****************************************************
    リンク処理
  *****************************************************/

  // #リンクは無効
  $('a[href="#"]').click(function(){
    return false;
  });

  spMenu();
  //matchHeightInit();
  acordionInit();
});

/*****************************************************
  ロード処理
*****************************************************/
$(window).on("load",function(){
});

/*****************************************************
  リサイズ処理
*****************************************************/
var chCntW;
$(window).on("load resize",function(){
  clientW = window.innerWidth;
  clientH = window.innerHeight;
  if(chCntW != clientW){
    chCntW = clientW;
  }
});

/*****************************************************
  スクロール処理
*****************************************************/
$(window).on("load resize scroll",function(){
});



/*****************************************************
  関数
*****************************************************/


function spMenu() {
  $(".l-header_menu a").click(function() {
    $(this).toggleClass("is-active");
    $(".l-header_nav").slideToggle(200);
    return false;
  });

  $(".l-header_nav dt a").click(function() {
    $(this).toggleClass("is-active");
    $(this).closest("dl").find("dd").slideToggle(200);
    return false;
  });
}

function acordionInit() {
  $(".c-acordionList dt a").click(function() {
    $(this).toggleClass("is-active");
    $(this).closest("dl").find("dd").slideToggle(200);
    return false;
  });
}

function matchHeightInit() {
  //targetを配列に格納
  var matchTarget = ['.js-matchHeight','.js-matchHeight02','.js-matchHeight03', '.js-matchHeight04']
  //格納した配列を回す
  $.each(matchTarget, function(i,val) {
    $(val).matchHeight();
  });
}
