// パッケージインポート
var gulp = require("gulp");

// ディレクトリ設定
var dir = {
    src: '_src/', // _srcフォルダ置き換え
    dist: 'dist', // distフォルダ置き換え
    guide: '_styleguide' // styleguideフォルダ置き換え
}

// Plumberインポート（エラー時に監視を止めない）
var plumber = require('gulp-plumber');

// Sassインポート
var sass = require('gulp-sass');
var cssnext = require('gulp-cssnext');

// Sassディレクトリ設定
var paths = {
  scss: dir.src + '/sp/bilingual/_shared/_sass/',
  css: dir.src + '/sp/bilingual/_shared/css/',
  guidecss: dir.guide + '/css/'
}

// Sass設定
gulp.task('scss', function() {
  return gulp.src(paths.scss + '/{,**/}*.scss')
  	.pipe(plumber())
    .pipe(sass({outputStyle: 'expanded'}))
  //.on('error', function(err) {
      //console.log(err.message);
    //})
    .pipe(cssnext({
        browsers: 'last 4 versions'
    }))
    .pipe(gulp.dest(paths.css))
});

// Frontnoteインポート
var frontNote = require("gulp-frontnote");

// スタイルガイド
gulp.task('frontnote', function() {
	gulp.src(paths.guidecss + '/{,**/}*.css')
		.pipe(frontNote({
			out: dir.guide + '/guide',
			//template: './my-template',
			//overview: './overview.md',
			includePath: 'assets/**/*'
		}));
});

// BrowserSyncインポート
var browserSync = require('browser-sync');

// BrowserSyncの設定
gulp.task('bs', function() {
  return browserSync.init({
    server: {
      notify: false,
      baseDir: dir.src,
      directory: true,
      // index: "index.html"
    },
    ghostMode: {
      location: true
    }
  });
});

// Reload all Browsers
gulp.task('reload', function () {
	return gulp.src([dir.src + '/{,**/}*.html'],{ base: dir.src })
    .pipe(browserSync.reload({
      stream: true
    }));
});

// Watchインポート
var watch = require('gulp-watch');
 
// ファイル更新監視
gulp.task('watch', function() {
    // scssの監視
    gulp.watch([ 
        dir.src + '/{,**/}*.scss' // 対象ファイル
    ],['scss']); // 実行タスク（css開発用）
    gulp.watch([ 
        dir.src + '/{,**/}*.jade' // 対象ファイル
    ],['jade']); // 実行タスク（jade開発用）
    gulp.watch([
        dir.src + '/{,**/}*.html', // 対象ファイル
        dir.src + '/{,**/}*.css',
        dir.src + '/{,**/}*.js'
    ],['reload']); // 実行タスク（scssファイル以外が更新されたタイミングでブラウザを自動更新）
});

// Imageminインポート
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var optipng = require('imagemin-optipng');
var mozjpeg  = require('imagemin-mozjpeg');
var gifsicle = require('imagemin-gifsicle');
var svgo = require('imagemin-svgo');

// 画像を圧縮
gulp.task('imagemin', function () {
    return gulp.src( dir.src + '/{,**/}*.{png,jpg,gif,svg}' ) // 読み込みファイル
    .pipe(imagemin([
       pngquant({
         quality: '65-80',
         speed: 1,
         floyd:0
       }),
       mozjpeg({
         quality:85,
         progressive: true
       }),
       imagemin.svgo(),
       imagemin.optipng(),
       imagemin.gifsicle()
     ]
    ))
    .pipe(gulp.dest( dir.dist )); // 書き出し先
});

// Uglifyインポート
var uglify = require('gulp-uglify');

// JSを圧縮
gulp.task('jsmin', function() {
    return gulp.src( dir.src + '/{,**/js/}{,**/}*.js' ) // 読み込みファイル
    .pipe(plumber())
    .pipe(uglify({output:{comments: /^!/}}))
    .pipe(gulp.dest( dir.dist )); // 書き出し先
});

// CleanCssインポート
var cleanCSS = require('gulp-clean-css');

// CSSを圧縮
gulp.task('cssmin', function() {
    //return gulp.src( paths.css + '{,**/}*.{css}')
    return gulp.src( dir.src + '/{,**/css/}{,**/}*.css' ) // 読み込みファイル
    .pipe(cleanCSS())
    .pipe(gulp.dest( dir.dist )); // 書き出し先
});

// ファイルのコピー
gulp.task('copy', function () {
    return gulp.src([
        dir.src + '/{,**/}*.html'
        //dir.src + '/{,**/}*.css',
        //dir.src + '/{,**/}*.js',
        //dir.src + '/{,**/}*.svg'
    ],{ base: dir.src })
    .pipe(gulp.dest(dir.dist));
});

// Cleanインポート
var clean = require('gulp-clean');
 
// 不要なファイルを削除する
// distフォルダ内を一度全て削除する
gulp.task('clean-dist', function () {
    return gulp.src([
        //dir.dist
        'dist'
    ], {read: false} )
    .pipe(clean());
});

// スプライト画像の生成データを全て削除する
gulp.task('clean-sprite', function () {
    return gulp.src( [
        dir.dist + '/{,**/}sprite-*.png', // 乱数付きのスプライト画像
        dir.dist + '/{,**/}sprite' // スプライト画像生成フォルダ
    ], {read: false} )
    .pipe(clean());
});

// Jadeインポート
var jade = require('gulp-jade');

// JadeをHTMLに変換
gulp.task('jade', function() {
  return gulp.src([dir.src + '/{,**/}*.jade'],{ base: dir.src + '/jade' })

    .pipe(plumber())

// .JadeのHTML変換実行
    .pipe(jade({pretty: true}))

//　書き出し先は「_src」ディレクトリ
    .pipe(gulp.dest(dir.src))

// Minifyしない場合は下記2行をコメントアウト
 //.pipe($.prettify())
 //.pipe(gulp.dest(dir.src))

});

// 開発用タスク（各ファイル監視してビルド）
gulp.task('default', [
    'bs',
    'watch'
]);

// RunSequenceインポート
var runSequence = require('run-sequence');

// 納品用タスク（タスク処理を指定した順番で実行）
gulp.task('dist', function(callback) {
  return runSequence( // タスクを直列処理
    'clean-dist',
    ['jade','scss','imagemin'],
    ['cssmin','jsmin'],
    'copy',
    'clean-sprite',
    callback
  );
});

